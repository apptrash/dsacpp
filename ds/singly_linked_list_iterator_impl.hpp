template <typename T>
SinglyLinkedListIterator<T>::SinglyLinkedListIterator(
    SinglyLinkedListNode<T>* node) noexcept : node_(node) {}

template <typename T>
T& SinglyLinkedListIterator<T>::operator*() noexcept {
  return node_->item;
}

template <typename T>
SinglyLinkedListIterator<T> SinglyLinkedListIterator<T>::operator++() noexcept {
  node_ = node_->next;
  return *this;
}

template <typename T>
SinglyLinkedListIterator<T> SinglyLinkedListIterator<T>::operator++(int) noexcept {
  SinglyLinkedListIterator<T> iterator(node_);
  node_ = node_->next;
  return iterator;
}

template <typename T>
bool SinglyLinkedListIterator<T>::operator==(
    SinglyLinkedListIterator<T> other) const noexcept {
  return node_ == other.node_;
}

template <typename T>
bool SinglyLinkedListIterator<T>::operator!=(
    SinglyLinkedListIterator<T> other) const noexcept {
  return !operator==(other);
}
