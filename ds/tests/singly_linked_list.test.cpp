#include "../singly_linked_list.h"
#include <gtest/gtest.h>

namespace dsacpp {

TEST(Constructor, Default) {
  SinglyLinkedList<int> list;

  ASSERT_EQ(list.Size(), 0);
  ASSERT_EQ(list.begin(), list.end());
}

TEST(Constructor, Count) {
  SinglyLinkedList<int> list(10);

  ASSERT_EQ(list.Size(), 10);
  int counter = 0;
  for (auto item : list) {
    ASSERT_EQ(item, 0);
    ++counter;
  }

  ASSERT_EQ(counter, 10);
}

TEST(Constructor, InitializerList) {
  SinglyLinkedList<int> list { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

  ASSERT_EQ(list.Size(), 10);

  int counter = 0;
  for (auto item : list) {
    ASSERT_EQ(item, counter + 1);
    ++counter;
  }

  ASSERT_EQ(counter, 10);
}

TEST(Constructor, EmptyInitializerList) {
  std::initializer_list<int> init_list;
  SinglyLinkedList<int> list(init_list);

  ASSERT_EQ(list.Size(), 0);
  ASSERT_EQ(list.begin(), list.end());
}

TEST(Constructor, CountWithItem) {
  SinglyLinkedList<int> list(10, -2);

  ASSERT_EQ(list.Size(), 10);
  int counter = 0;
  for (auto item : list) {
    ASSERT_EQ(item, -2);
    ++counter;
  }

  ASSERT_EQ(counter, 10);
}

TEST(Constructor, Iterators) {
  const int kArraySize = 5;
  int array[] = { 1, 2, 3, 4, 5 };

  SinglyLinkedList<int> list(array, array + kArraySize);

  ASSERT_EQ(list.Size(), kArraySize);

  int i = 1;
  for (auto item : list) {
    ASSERT_EQ(item, i);
    ++i;
  }
  ASSERT_EQ(i, kArraySize + 1);
}

TEST(Constructor, EmptyRangeIterators) {
  std::initializer_list<int> init_list;
  SinglyLinkedList<int> list(init_list.begin(), init_list.end());
  ASSERT_EQ(list.Size(), 0);
  ASSERT_EQ(list.begin(), list.end());
}

TEST(CopyConstructor, Common) {
  SinglyLinkedList<int> list({1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
  SinglyLinkedList<int> copy(list);

  ASSERT_EQ(list.Size(), 10);
  ASSERT_EQ(copy.Size(), 10);

  int i = 1;
  for (auto item : copy) {
    ASSERT_EQ(item, i);
    ++i;
  }
  ASSERT_EQ(i, 11);

  list.Clear();

  i = 1;
  for (auto item : copy) {
    ASSERT_EQ(item, i);
    ++i;
  }
  ASSERT_EQ(i, 11);
  ASSERT_EQ(copy.Size(), 10);
}

TEST(CopyConstructor, Empty) {
  SinglyLinkedList<int> list;
  SinglyLinkedList<int> copy(list);

  ASSERT_EQ(list.Size(), 0);
  ASSERT_EQ(copy.Size(), 0);
  ASSERT_EQ(copy.begin(), copy.end());
}

TEST(MoveConstructor, Common) {
  SinglyLinkedList<int> list({1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
  SinglyLinkedList<int> copy(std::move(list));

  ASSERT_EQ(list.Size(), 0);
  ASSERT_EQ(copy.Size(), 10);
  ASSERT_EQ(list.begin(), list.end());

  int i = 1;
  for (auto item : copy) {
    ASSERT_EQ(item, i);
    ++i;
  }
  ASSERT_EQ(i, 11);
}

TEST(PushBack, Common) {
  SinglyLinkedList<int> list;

  list.PushBack(1);
  ASSERT_EQ(list.Size(), 1);
  list.PushBack(2);
  ASSERT_EQ(list.Size(), 2);

  auto begin = list.begin();
  ASSERT_EQ(*begin, 1);
  ++begin;
  ASSERT_EQ(*begin, 2);
  ++begin;
  ASSERT_EQ(begin, list.end());
}

TEST(PushFront, Common) {
  SinglyLinkedList<int> list;

  list.PushFront(1);
  ASSERT_EQ(list.Size(), 1);
  list.PushFront(2);
  ASSERT_EQ(list.Size(), 2);

  auto begin = list.begin();
  ASSERT_EQ(*begin, 2);
  ++begin;
  ASSERT_EQ(*begin, 1);
  ++begin;
  ASSERT_EQ(begin, list.end());
}

TEST(Clear, Common) {
  SinglyLinkedList<int> list({1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
  list.Clear();
  ASSERT_EQ(list.Size(), 0);
  ASSERT_EQ(list.begin(), list.end());
}

TEST(Remove, Empty) {
  SinglyLinkedList<int> list;
  ASSERT_FALSE(list.Remove(1));
  ASSERT_EQ(list.Size(), 0);
}

TEST(Remove, OneItem) {
  SinglyLinkedList<int> list;

  list.PushBack(1);

  ASSERT_TRUE(list.Remove(1));
  ASSERT_EQ(list.Size(), 0);
}

TEST(Remove, Head) {
  SinglyLinkedList<int> list;

  list.PushBack(1);
  list.PushBack(2);

  ASSERT_TRUE(list.Remove(1));
  ASSERT_EQ(list.Size(), 1);

  auto begin = list.begin();
  ASSERT_EQ(*begin, 2);
  ++begin;
  ASSERT_EQ(begin, list.end());
}

TEST(Remove, Tail) {
  SinglyLinkedList<int> list;

  list.PushBack(1);
  list.PushBack(2);

  ASSERT_TRUE(list.Remove(2));
  ASSERT_EQ(list.Size(), 1);

  auto begin = list.begin();
  ASSERT_EQ(*begin, 1);
  ++begin;
  ASSERT_EQ(begin, list.end());
}

TEST(Remove, InTheMiddle) {
  SinglyLinkedList<int> list;

  list.PushBack(1);
  list.PushBack(2);
  list.PushBack(3);

  ASSERT_TRUE(list.Remove(2));
  ASSERT_EQ(list.Size(), 2);

  auto begin = list.begin();
  ASSERT_EQ(*begin, 1);
  ++begin;
  ASSERT_EQ(*begin, 3);
  ++begin;
  ASSERT_EQ(begin, list.end());
}

TEST(Remove, NotExists) {
  SinglyLinkedList<int> list;

  list.PushBack(1);
  ASSERT_FALSE(list.Remove(2));
  ASSERT_EQ(list.Size(), 1);

  auto begin = list.begin();
  ASSERT_EQ(*begin, 1);
  ++begin;
  ASSERT_EQ(begin, list.end());
}

TEST(Front, Common) {
  SinglyLinkedList<int> list;

  list.PushBack(1);
  ASSERT_EQ(list.Front(), 1);
  list.PushBack(2);
  ASSERT_EQ(list.Front(), 1);
}

TEST(Back, Common) {
  SinglyLinkedList<int> list;

  list.PushBack(1);
  ASSERT_EQ(list.Back(), 1);
  list.PushBack(2);
  ASSERT_EQ(list.Back(), 2);
}

TEST(Contains, Common) {
  SinglyLinkedList<bool> list;

  list.PushBack(true);
  ASSERT_TRUE(list.Contains(true));
  ASSERT_FALSE(list.Contains(false));
  ASSERT_EQ(list.Size(), 1);
}

} // namespace dsacpp
