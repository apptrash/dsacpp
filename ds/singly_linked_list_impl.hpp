template <typename T>
SinglyLinkedList<T>::SinglyLinkedList(int count) : size_(count) {
  if (count < 1) {
    return;
  }

  auto iter = head_ = new SinglyLinkedListNode<T>();
  for(int i = 1; i < count; ++i) {
    iter->next = new SinglyLinkedListNode<T>();
    iter = iter->next;
  }

  tail_ = iter;
  iter->next = nullptr;
}

template <typename T>
SinglyLinkedList<T>::SinglyLinkedList(int count, const T& item)
  : size_(count) {
  if (count < 1) {
    return;
  }

  auto iter = head_ = new SinglyLinkedListNode<T>({item});

  for(int i = 1; i < count; ++i) {
    iter->next = new SinglyLinkedListNode<T>({item});
    iter = iter->next;
  }

  tail_ = iter;
  iter->next = nullptr;
}

template <typename T>
SinglyLinkedList<T>::SinglyLinkedList(std::initializer_list<T> init)
  : size_(0) {
  if (init.begin() == init.end()) {
    return;
  }

  auto iter = head_ = new SinglyLinkedListNode<T>({*init.begin()});
  auto other_iter = init.begin();
  ++other_iter;
  ++size_;

  while (other_iter != init.end()) {
    iter->next = new SinglyLinkedListNode<T>({*other_iter});
    ++other_iter;
    iter = iter->next;
    ++size_;
  }

  tail_ = iter;
  tail_->next = nullptr;
}

template <typename T>
template <typename Iterator>
SinglyLinkedList<T>::SinglyLinkedList(Iterator begin, Iterator end)
  : size_(0) {
  if (begin == end) {
    return;
  }

  auto iter = head_ = new SinglyLinkedListNode<T>({*begin});
  auto range_iter = begin;
  ++range_iter;
  ++size_;

  while (range_iter != end) {
    iter->next = new SinglyLinkedListNode<T>({*range_iter});
    ++range_iter;
    iter = iter->next;
    ++size_;
  }

  tail_ = iter;
  tail_->next = nullptr;
}

template <typename T>
SinglyLinkedList<T>::SinglyLinkedList(const SinglyLinkedList<T>& other)
  : size_(other.size_) {
  if (other.size_ == 0) {
    return;
  }

  auto iter = head_ = new SinglyLinkedListNode<T>({other.head_->item});
  auto other_iter = other.head_;
  while (other_iter->next != nullptr) {
    iter->next = new SinglyLinkedListNode<T>({other_iter->next->item});
    other_iter = other_iter->next;
    iter = iter->next;
  }

  tail_ = iter;
  tail_->next = nullptr;
}

template <typename T>
SinglyLinkedList<T>::SinglyLinkedList(SinglyLinkedList&& other)
  noexcept : head_(other.head_),
             tail_(other.tail_),
             size_(other.size_) {
  other.head_ = other.tail_ = nullptr;
  other.size_ = 0;
}

template <typename T>
SinglyLinkedList<T>::~SinglyLinkedList() {
  while (head_ != nullptr) {
    auto node = head_;
    head_ = head_->next;
    delete node;
  }
}

template <typename T>
void SinglyLinkedList<T>::PushBack(const T& item) {
  auto node = new SinglyLinkedListNode<T>({item, nullptr});

  if (head_ == nullptr) {
    head_ = tail_ = node;
  } else {
    tail_->next = node;
    tail_ = tail_->next;
  }

  ++size_;
}

template <typename T>
void SinglyLinkedList<T>::PushFront(const T& item) {
  auto node = new SinglyLinkedListNode<T>({item, head_});
  if (head_ == nullptr) {
    tail_ = node;
  }

  head_ = node;
  ++size_;
}

template <typename T>
void SinglyLinkedList<T>::Clear() {
  while (head_ != nullptr) {
    auto node = head_;
    head_ = head_->next;
    delete node;
  }

  size_ = 0;
}

template <typename T>
bool SinglyLinkedList<T>::Remove(const T& item) {
  if (head_ == nullptr) {
    return false;
  }

  auto node = head_;
  if (node->item == item) {
    head_ = head_->next;
    delete node;
    --size_;
    return true;
  }

  while (node->next != nullptr && !(node->next->item == item)) {
    node = node->next;
  }

  if (node->next != nullptr) {
    if (node->next == tail_) {
      delete tail_;
      tail_ = node;
      tail_->next = nullptr;
    } else {
      auto rm_node = node->next;
      node->next = node->next->next;
      delete rm_node;
    }

    --size_;
    return true;
  }

  return false;
}

template <typename T>
T& SinglyLinkedList<T>::Front() noexcept {
  return head_->item;
}

template <typename T>
const T& SinglyLinkedList<T>::Front() const noexcept {
  return head_->item;
}

template <typename T>
T& SinglyLinkedList<T>::Back() noexcept {
  return tail_->item;
}

template <typename T>
const T& SinglyLinkedList<T>::Back() const noexcept {
  return tail_->item;
}

template <typename T>
bool SinglyLinkedList<T>::Contains(const T& item) const {
  auto node = head_;
  while (node != nullptr && !(node->item == item)) {
    node = node->next;
  }

  return node != nullptr;
}

template <typename T>
int SinglyLinkedList<T>::Size() const noexcept {
  return size_;
}

template <typename T>
SinglyLinkedListIterator<T> SinglyLinkedList<T>::begin() noexcept {
  return SinglyLinkedListIterator<T>(head_);
}

template <typename T>
SinglyLinkedListIterator<T> SinglyLinkedList<T>::end() noexcept {
  return SinglyLinkedListIterator<T>(nullptr);
}
