#ifndef DSACPP_DS_SINGLY_LINKED_LIST_H_
#define DSACPP_DS_SINGLY_LINKED_LIST_H_

#include <initializer_list>
#include "singly_linked_list_node.h"
#include "singly_linked_list_iterator.h"

namespace dsacpp {

template <typename T>
class SinglyLinkedList final {
 public:
  SinglyLinkedList() = default;
  explicit SinglyLinkedList(int count);
  SinglyLinkedList(int count, const T& item);
  SinglyLinkedList(std::initializer_list<T> init);
  template <typename Iterator>
  SinglyLinkedList(Iterator begin, Iterator end);
  SinglyLinkedList(const SinglyLinkedList& other);
  SinglyLinkedList(SinglyLinkedList&& other) noexcept;
  ~SinglyLinkedList();

  void PushBack(const T& item);
  void PushFront(const T& item);
  void Clear();
  bool Remove(const T& item);
  T& Front() noexcept;
  const T& Front() const noexcept;
  T& Back() noexcept;
  const T& Back() const noexcept;
  bool Contains(const T& item) const;
  int Size() const noexcept;

  SinglyLinkedListIterator<T> begin() noexcept;
  SinglyLinkedListIterator<T> end() noexcept;
 private:
  SinglyLinkedListNode<T>* head_ = nullptr;
  SinglyLinkedListNode<T>* tail_;
  int size_ = 0;
};

#include "singly_linked_list_impl.hpp"

}; // namespace dsacpp

#endif  // DSACPP_DS_SINGLY_LINKED_LIST_H_
