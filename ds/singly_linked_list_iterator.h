#ifndef DSACPP_DS_SINGLY_LINKED_LIST_ITERATOR_H_
#define DSACPP_DS_SINGLY_LINKED_LIST_ITERATOR_H_

#include "singly_linked_list_node.h"

namespace dsacpp {

template <typename T>
class SinglyLinkedListIterator final {
 public:
  explicit SinglyLinkedListIterator(SinglyLinkedListNode<T>* node) noexcept;

  T& operator*() noexcept;
  SinglyLinkedListIterator<T> operator++() noexcept;
  SinglyLinkedListIterator<T> operator++(int) noexcept;
  bool operator==(SinglyLinkedListIterator<T> other) const noexcept;
  bool operator!=(SinglyLinkedListIterator<T> other) const noexcept;
 private:
  SinglyLinkedListNode<T>* node_;
};

#include "singly_linked_list_iterator_impl.hpp"

} // namespace dsacpp

#endif  // DSACPP_DS_SINGLY_LINKED_LIST_ITERATOR_H_
