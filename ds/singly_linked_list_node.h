#ifndef DSACPP_DS_SINGLY_LINKED_LIST_NODE_H_
#define DSACPP_DS_SINGLY_LINKED_LIST_NODE_H_

namespace dsacpp {

template <typename T>
struct SinglyLinkedListNode {
  T item;
  SinglyLinkedListNode* next;
};

}

#endif  // DSACPP_DS_SINGLY_LINKED_LIST_NODE_H_
