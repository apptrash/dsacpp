# dsacpp library #

This library contains native implementation of data structures and algortihms
described in DSA book (http://dotnetslackers.com/)

## How to run tests ##
  1. Go to dsacpp folder
  2. cd ..
  3. mkdir build
  4. cd build
  5. cmake ../dsacpp/
  6. make
  7. ctest .
